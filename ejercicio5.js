// Ejercicio no.5 Enteros invertidos
// Dado un entero de 32 bits x, regresa los dígitos invertidos de x. 
// Si invertir x causa que se salga del rango de 32 bits [-231, 231 – 1] entonces regresar 0.

// Ejemplo 1:
// Input: x = 123
// Output: 321

// Ejemplo 2:
// Input: x = -123
// Output: -321

// Ejemplo 3:
// Input: x = 120
// Output: 21

// Ejemplo 4:
// Input: x = 0
// Output: 0
